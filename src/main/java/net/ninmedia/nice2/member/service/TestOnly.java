package net.ninmedia.nice2.member.service;

import net.ninmedia.nice2.core.util.ConfigUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

import java.lang.invoke.MethodHandles;

@Component
public class TestOnly implements ApplicationRunner {
    @Autowired
    ConfigUtil configUtil;

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Override
    public void run(ApplicationArguments args) throws Exception {
        log.info("Config UTIL: "+configUtil.getConfig("api.base.url.member"));
    }
}
